<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = ['DESCRICAO'];
    protected $guarded = ['COD_CATEGORIA', 'created_at', 'update_at'];
    protected $primaryKey = 'COD_CATEGORIA';

    protected $table = 'categoria';
}
