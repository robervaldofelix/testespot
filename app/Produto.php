<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = ['DESCRICAO','COD_CATEGORIA'];
    protected $guarded = ['COD_PRODUTO', 'created_at', 'update_at'];
    protected $primaryKey = 'COD_PRODUTO';

    protected $table = 'produto';
}
