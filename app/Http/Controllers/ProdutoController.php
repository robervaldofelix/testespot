<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use App\Categoria;
USE DB;
class ProdutoController extends Controller
{   
    //Index Pagina de Produtos
    public function index() {

        $produtos = DB::table('produto')
                    ->join('categoria', 'categoria.COD_CATEGORIA','=','produto.COD_CATEGORIA')
                    ->select('produto.COD_PRODUTO','produto.DESCRICAO','produto.COD_CATEGORIA','categoria.DESCRICAO as DESC_CATEGORIA')
                    ->get();

        $total = Produto::all()->count();
        return view('list-produtos', compact('produtos', 'total'));
    }

    //View formulario Cadastro de Produto
    public function create() {
        $categorias = Categoria::all();
        return view('include-produto', compact('categorias'));
    }

    //Cadastrando Produtos No Banco
    public function store(Request $request) {
        $product = new Produto;
        $product->DESCRICAO = $request->description;
        $product->COD_CATEGORIA =  $request->COD_CATEGORIA;
        $product->save();
        return redirect()->route('product.index')->with('message', 'Produto criado com sucesso!');
    }

    public function show($id) {
        //
    }

    //View Formulario Editar Produtos
    public function edit($id) {
        $product = Produto::findOrFail($id);
        $categorias = Categoria::all();
        return view('alter-produto', compact('product','categorias'));
    }

    //Update dos Produtos no banco
    public function update(Request $request, $id) {
        $product = Produto::findOrFail($id);
        $product->DESCRICAO = $request->description;
        $product->COD_CATEGORIA = $request->COD_CATEGORIA;
        $product->save();
        return redirect()->route('product.index')->with('message', 'Produto alterado com sucesso!');
    }

    //Deletar Produto
    public function destroy($id) {
        $product = Produto::findOrFail($id);
        $product->delete();
        return redirect()->route('product.index')->with('message', 'Produto excluído com sucesso!');
    }

}
